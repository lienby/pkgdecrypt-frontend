# Releases

# v0.2

You can now drag .pkg files onto the application to extract them.  
You can also set this application as the default app for using .pkg files.  
:D still dont know how to do that programmically though.  

Win32:   
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-0.2.zip  
Win32-Install:   
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-install-0.2.exe  
Linux64:   
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-linux64-0.2.tar.gz   

# v0.1.1
Linux may require libssl-dev.

+ Fixes the problem on windows where it would say "pkg file not found"

Win32: 
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-0.1.1.zip    
Win32-Install:  
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-install-0.1.1.zip    
Linux64:  
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-linux64-0.1.1.tar.gz   

# v0.1
Linux requires libssl-dev.

Win32: 
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-0.1.zip    
Win32-Install:  
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-win32-install-0.1.zip    
Linux64: 
https://bitbucket.org/SilicaAndPina/pkgdecrypt-frontend/downloads/pkgdecrypt-frontend-linux64-0.1.tar.gz    

# Readme
A Gui For St4rk's PKG Extractor

Linux Version May Require libssl-dev 
(sudo apt-get install libssl-dev)

Written in python tkinter however i did use PAGE to create the GUI's easier.

For noobs: No this will NOT enable piracy, no you cannot convert pkg to psvimgtools either (yet) this might come eventurally if we get teh ability to do a scePromote on pc? and no, you can not get 3.63 games working on 3.60 please dont ask these things.
